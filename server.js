var express = require('express');

// Constants
var PORT = 8888;

// App
var app = express();

app.get('/', function (req, res) {
    res.sendfile('index.html');
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
